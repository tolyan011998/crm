import Vue from 'vue'
import Vuelidate from 'vuelidate'
import Paginate from 'vuejs-paginate'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import dateFilter from "./filters/date.filter";
import currencyFilter from "./filters/currency.filter";
import tooltipDirective from './directives/tooltip.directive'
import messagePlugin from './utils/message.plugin'
import Loader from './components/app/Loader'
import 'materialize-css/dist/js/materialize.min'

import firebase from 'firebase/app'
import 'firebase/auth';
import 'firebase/database';

Vue.config.productionTip = false

Vue.use(messagePlugin)
Vue.use(Vuelidate)
Vue.filter('date', dateFilter)
Vue.filter('currency', currencyFilter)
Vue.directive('tooltip', tooltipDirective)
Vue.component('Loader', Loader)
Vue.component('Paginate', Paginate)

firebase.initializeApp({
  apiKey: "AIzaSyB9GhQa4RITrt05fWjMWpKEefHY6-kJVPU",
  authDomain: "crm-tolya.firebaseapp.com",
  databaseURL: "https://crm-tolya.firebaseio.com",
  projectId: "crm-tolya",
  storageBucket: "crm-tolya.appspot.com",
  messagingSenderId: "757247824178",
  appId: "1:757247824178:web:4d9379b4bf3dc91e3d072b",
  measurementId: "G-7EKTXC310C"
})

let app

firebase.auth().onAuthStateChanged(() =>{
  if (!app) {
    app = new Vue({
      router,
      store,
      render: h => h(App)
    }).$mount('#app')
  }
})

